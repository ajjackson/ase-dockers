FROM registry.gitlab.com/ase/ase:ase-build-tools

RUN apt-get install -y ninja-build

USER ase

ARG VERSION=22.0.4
ARG MOPAC=mopac-${VERSION}
ARG TARFILE=v${VERSION}.tar.gz
ARG BUILDDIR=/home/ase/build
ARG CALCDIR=/home/ase/calculators/mopac

RUN mkdir -p ${BUILDDIR}
WORKDIR ${BUILDDIR}

RUN wget https://github.com/openmopac/mopac/archive/refs/tags/${TARFILE}
RUN tar -xf ${TARFILE}
WORKDIR ${BUILDDIR}/${MOPAC}

RUN cmake -B${BUILDDIR} -GNinja -DBLA_VENDOR=Generic .
RUN cmake --build ${BUILDDIR}

# Copy relevant stuff to clean directories
RUN mkdir -p ${CALCDIR}/bin
RUN mkdir -p ${CALCDIR}/lib
RUN ["/bin/bash", "-c", "cp ${BUILDDIR}/{mopac,mopac-makpol,mopac-param} ${CALCDIR}/bin/"]
RUN cp ${BUILDDIR}/libmopac* ${CALCDIR}/lib
