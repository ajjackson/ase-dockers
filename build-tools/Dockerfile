FROM python:3.12-slim
# Note: cmake and pkg-config are required for lammpslib, psi4, and KIMCalculator
# Note: xxd utility is required by KIM API (for KIMCalculator)
RUN apt-get update && apt-get install -y wget build-essential gfortran git cmake pkg-config xxd
RUN apt-get install -y openmpi-bin libopenmpi-dev
RUN apt-get install -y libxc-dev libblas-dev liblapack-dev
RUN apt-get install -y libgsl27  # octopus
RUN apt-get install -y libhdf5-dev libnetcdf-dev libnetcdff-dev
RUN apt-get install -y libfftw3-dev  # openmx, octopus
RUN apt-get install -y libscalapack-openmpi-dev  # openmx
RUN apt-get install -y libnetcdf-dev libnetcdff-dev libhdf5-dev  # abinit
RUN apt-get install -y xsltproc  # exciting
RUN apt-get install -y nano vim  # editors for interactive hacking
RUN apt-get install -y libgomp1  # mopac

RUN apt-get install -y libeigen3-dev  # psi4
RUN apt-get install -y libboost-dev  # psi4

# Set up the ase user which will do most installation work from now:
RUN adduser ase --disabled-password --gecos ""  #  gecos ==> non-interactive
USER ase
RUN mkdir -p /home/ase/build
RUN mkdir -p /home/ase/calculators

USER root
