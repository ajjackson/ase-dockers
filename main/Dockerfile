FROM registry.gitlab.com/ase/ase:ase-debian-calculators

# RUN pip install pycodcif  # Requires swig (many packages)
# RUN adduser ase --disabled-password --gecos ""  #  gecos ==> non-interactive
RUN apt-get install -y gettext  # For building UI translations
RUN passwd --delete root
# Are we entirely sure that root access has been disabled?
# RUN python -c "print('hello');print('hello')" | passwd root
USER ase
WORKDIR /home/ase
ENV PATH /home/ase/.local/bin:$PATH

RUN pip install psycopg2-binary netCDF4 pymysql cryptography
RUN pip install scipy numpy matplotlib sphinx sphinx_rtd_theme pillow
RUN pip install pytest pytest-xdist pytest-xvfb pytest-cov
# kimpy (required for KIM calculator) requires pybind11
RUN pip install pybind11
RUN pip install spglib pyberny

# Since we have all the Python dependencies, we take this opportunity
# to also install the classic Python codes:
RUN pip install gpaw asap3
RUN pip install flask
RUN gpaw install-data . --register
# We obviously want to provide our own ASE, so we uninstall the ASE from
# gpaw/asap3 in order to ensure that things are good.
RUN pip uninstall -y ase

# XXX Remove: mypy, flake8
RUN pip install pydantic pint
RUN pip install scikit-image
RUN pip install pyamg

# Not so nice to depend on exciting for main docker, but
# probably not an infrastructure burden
RUN pip install excitingtools
